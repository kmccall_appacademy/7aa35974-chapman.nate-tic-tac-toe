class Board
  attr_reader :grid

  def initialize(grid=nil)
    # Allows user to pass in their own grid
    if grid != nil
      @grid = grid
    else
      @grid = Array.new(3) {Array.new(3) {grid}}
    end
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] != :X && @grid[pos[0]][pos[1]] != :O
  end

end
